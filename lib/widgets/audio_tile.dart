import 'package:flutter/material.dart';

class AudioTile extends StatelessWidget {
  const AudioTile({
    super.key,
    required this.isPlaying,
    required this.duration,
    required this.isSelected,
    required this.label,
    this.onTap,
  });

  final void Function()? onTap;
  final bool isPlaying;
  final String label;
  final String duration;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amberAccent,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              children: [
                IconButton(
                    padding: EdgeInsets.zero,
                    style: IconButton.styleFrom(
                        visualDensity: VisualDensity.compact,
                        padding: EdgeInsets.zero),
                    onPressed: () async {},
                    icon: Icon(isSelected
                        ? Icons.pause_rounded
                        : Icons.play_arrow_rounded)),
                Expanded(
                  child: Text(
                    label,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Text(
                  duration,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
