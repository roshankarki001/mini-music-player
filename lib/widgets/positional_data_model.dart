class PositionData {
  final Duration position;
  final Duration bufferPosition;
  final Duration duration;

  const PositionData(
      {required this.position,
      required this.bufferPosition,
      required this.duration});
}
