import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:media_player/widgets/audio_model.dart';
import 'package:media_player/widgets/custom_mini_player.dart';
import 'package:media_player/widgets/just_audio_tile.dart';
import 'package:miniplayer/miniplayer.dart';

class MediaPlayerJustAudio extends StatefulWidget {
  const MediaPlayerJustAudio({super.key});

  @override
  State<MediaPlayerJustAudio> createState() => _MediaPlayerJustAudioState();
}

class _MediaPlayerJustAudioState extends State<MediaPlayerJustAudio> {
  late final AudioPlayer audioPlayer;

  final MiniplayerController miniplayerController = MiniplayerController();

  late final List<AudioData> musicUrlList;

  Duration audioDuration = Duration.zero;
  Duration positionDuration = Duration.zero;
  bool isPlaying = false;

  @override
  void dispose() {
    audioPlayer.stop();
    audioPlayer.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    audioPlayer = AudioPlayer();
    musicUrlList = AudioData.listFromJson([
      {
        'path': 'assets/in_the_end.mp3',
        'artist': 'Linkin Park',
        'lable': 'In the End - Linkin park Official Video Full video on',
        'duration': '04:14',
      },
      {
        'path': 'assets/numb.mp3',
        'artist': 'Linkin Park',
        'lable': 'Numb - Linkin park Official Video',
        'duration': '03:24',
      },
      {
        'path': 'assets/battle_symphony.mp3',
        'artist': 'Linkin Park',
        'lable': 'Battle Symphony - Linkin park Official Video',
        'duration': '02:04',
      },
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            ListView.separated(
                itemBuilder: (context, index) {
                  return JustAudioTile(
                    onTap: () async {
                      try {
                        if (shouldPlayNewAudio(
                          audioPlayer: audioPlayer,
                          path: musicUrlList[index].path,
                        )) {
                          await audioPlayer.setAsset(musicUrlList[index].path!);
                          await audioPlayer.play();
                        } else if (audioPlayer.playing) {
                          await audioPlayer.pause();
                        } else {
                          await audioPlayer.play();
                        }
                      } catch (ex) {}
                    },
                    label: musicUrlList[index].lable!,
                    duration: musicUrlList[index].duration!,
                    audioPlayer: audioPlayer,
                    path: musicUrlList[index].path!,
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider(
                    color: Colors.grey,
                    height: 1,
                  );
                },
                itemCount: musicUrlList.length),
            CustomMiniPlayer(
              audioPlayer: audioPlayer,
              musicUrlList: musicUrlList,
            )
          ],
        ),
      ),
    );
  }

  bool shouldPlayNewAudio({required AudioPlayer audioPlayer, String? path}) {
    final audioPath =
        ((audioPlayer.sequence?.first as ProgressiveAudioSource?)?.uri.path)
            ?.split('/')
            .toList()
            .last;
    path = path?.split('/').toList().last;
    if (audioPlayer.audioSource == null || audioPath?.toLowerCase() != path) {
      return true;
    } else {
      return false;
    }
  }
}

String formatTime(Duration duration) {
  String twoDigits(int n) => n.toString().padLeft(2, '0');
  final hours = twoDigits(duration.inHours);
  final minutes = twoDigits(duration.inMinutes.remainder(60));
  final seconds = twoDigits(duration.inSeconds.remainder(60));
  return [
    if (duration.inHours > 0) hours,
    minutes,
    seconds,
  ].join(":");
}
