import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

class JustAudioTile extends StatelessWidget {
  const JustAudioTile({
    super.key,
    required this.label,
    required this.duration,
    required this.path,
    required this.audioPlayer,
    this.onTap,
  });
  final String label;
  final String duration;
  final String path;
  final AudioPlayer audioPlayer;
  final void Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PlayerState>(
      stream: audioPlayer.playerStateStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        final playerState = snapshot.data;
        final processingState = playerState?.processingState;
        final playing = playerState?.playing;
        final audioPath =
            ((audioPlayer.sequence?.first as ProgressiveAudioSource?)?.uri.path)
                ?.split('/')
                .toList()
                .last;
        final path = this.path.split('/').toList().last;
        return Container(
          color: Colors.amberAccent,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: onTap,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  children: [
                    if (audioPath?.toLowerCase() == path.toLowerCase()) ...[
                      if (!(playing ?? false) ||
                          audioPlayer.audioSource == null) ...[
                        const Icon(Icons.play_arrow_rounded),
                      ] else if (processingState !=
                          ProcessingState.completed) ...[
                        const Icon(Icons.pause_rounded)
                      ] else ...[
                        const Icon(Icons.play_arrow_rounded),
                      ],
                    ] else ...[
                      const Icon(Icons.play_arrow_rounded),
                    ],
                    const SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        label,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Text(
                      duration,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
