import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:media_player/widgets/positional_data_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';

class AudioScale extends StatefulWidget {
  final TextStyle? timeLabelTextStyle;
  final AudioPlayer audioPlayer;
  final double barHeight;
  final double thumbRadius;
  const AudioScale({
    super.key,
    required this.audioPlayer,
    this.timeLabelTextStyle,
    this.barHeight = 4,
    this.thumbRadius = 3,
  });

  @override
  State<AudioScale> createState() => _AudioScaleState();
}

class _AudioScaleState extends State<AudioScale> {
  Stream<PositionData> get _positionDataStream => Rx.combineLatest3(
      widget.audioPlayer.positionStream,
      widget.audioPlayer.bufferedPositionStream,
      widget.audioPlayer.durationStream,
      (position, bufferposition, duration) => PositionData(
          position: position,
          bufferPosition: bufferposition,
          duration: duration ?? Duration.zero));
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PositionData>(
        stream: _positionDataStream,
        builder: (context, snapshot) {
          var positonData = snapshot.data;
          return ProgressBar(
            progress: positonData?.position ?? Duration.zero,
            total: positonData?.duration ?? Duration.zero,
            buffered: positonData?.bufferPosition ?? Duration.zero,
            onSeek: widget.audioPlayer.seek,
            barHeight: widget.barHeight,
            thumbRadius: widget.thumbRadius,
            thumbColor: Colors.black,
            baseBarColor: Colors.red,
            bufferedBarColor: Colors.green,
            thumbCanPaintOutsideBar: true,
            thumbGlowColor: Colors.purple,
            progressBarColor: Colors.blue,
            timeLabelTextStyle: widget.timeLabelTextStyle ??
                const TextStyle(
                  fontSize: 12,
                ),
          );
        });
  }
}
