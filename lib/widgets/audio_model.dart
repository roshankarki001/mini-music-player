class AudioData {
  String? path;
  String? artist;
  String? lable;
  String? duration;

  AudioData({this.path, this.artist, this.lable, this.duration});

  AudioData.fromJson(Map<String, dynamic> json) {
    path = json['path'];
    artist = json['artist'];
    lable = json['lable'];
    duration = json['duration'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['path'] = path;
    data['artist'] = artist;
    data['lable'] = lable;
    data['duration'] = duration;
    return data;
  }

  static List<AudioData> listFromJson(List<dynamic>? json) {
    List<AudioData> attendanceList = [];
    for (var each in json ?? []) {
      var eachAttendance = AudioData.fromJson(each);
      attendanceList.add(eachAttendance);
    }
    return attendanceList;
  }
}
