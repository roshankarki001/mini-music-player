import 'package:flutter/material.dart';

class AlbumPlayerImage extends StatefulWidget {
  final bool isPlaying;
  final Widget child;
  const AlbumPlayerImage(
      {super.key, required this.child, required this.isPlaying});

  @override
  State<AlbumPlayerImage> createState() => _AlbumPlayerImageState();
}

class _AlbumPlayerImageState extends State<AlbumPlayerImage>
    with TickerProviderStateMixin {
  late AnimationController _recordController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _recordController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 10),
    )..repeat();
  }

  @override
  void dispose() {
    _recordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isPlaying) {
      _recordController.repeat();
    } else {
      _recordController.stop();
    }

    return RotationTransition(
      turns: Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(parent: _recordController, curve: Curves.linear),
      ),
      child: widget.child,
    );
  }
}
