import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:media_player/album_player.dart';
import 'package:miniplayer/miniplayer.dart';

class MediaPlayer extends StatefulWidget {
  const MediaPlayer({super.key});

  @override
  State<MediaPlayer> createState() => _MediaPlayerState();
}

class _MediaPlayerState extends State<MediaPlayer> {
  final AudioPlayer audioPlayer = AudioPlayer();
  final MiniplayerController miniplayerController = MiniplayerController();
  final List<Map<String, String>> musicUrlList = [
    {
      'path': 'in_the_end.mp3',
      'artist': 'Linkin Park',
      'lable': 'In the End - Linkin park Official Video Full video on',
      'duration': '04:14',
    },
    {
      'path': 'numb.mp3',
      'artist': 'Linkin Park',
      'lable': 'Numb - Linkin park Official Video',
      'duration': '03:24',
    },
    {
      'path': 'battle_symphony.mp3',
      'artist': 'Linkin Park',
      'lable': 'Battle Symphony - Linkin park Official Video',
      'duration': '02:04',
    },
  ];
  final ValueNotifier<int?> _selected = ValueNotifier<int?>(null);
  Duration audioDuration = Duration.zero;
  Duration positionDuration = Duration.zero;
  bool isPlaying = false;
  @override
  void dispose() {
    audioPlayer.stop();
    audioPlayer.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    audioPlayer.onDurationChanged.listen((event) {
      audioDuration = event;
      setState(() {});
    });
    audioPlayer.onPlayerStateChanged.listen((event) {
      isPlaying = event == PlayerState.playing;
      if (event == PlayerState.completed) {
        positionDuration = Duration.zero;
      }
      setState(() {});
    });

    audioPlayer.onPositionChanged.listen((event) {
      positionDuration = event;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            ListView.separated(
                itemBuilder: (context, index) {
                  return Container(
                    color: Colors.amberAccent,
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () async {
                          if (positionDuration == Duration.zero ||
                              _selected.value != index) {
                            await audioPlayer.stop();
                            await audioPlayer.play(
                                AssetSource(musicUrlList[index]['path']!));
                          } else {
                            if (isPlaying) {
                              await audioPlayer.pause();
                            } else {
                              await audioPlayer.resume();
                            }
                          }
                          _selected.value = index;
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: Row(
                            children: [
                              IconButton(
                                  padding: EdgeInsets.zero,
                                  style: IconButton.styleFrom(
                                      visualDensity: VisualDensity.compact,
                                      padding: EdgeInsets.zero),
                                  onPressed: () async {
                                    if (positionDuration == Duration.zero) {
                                      await audioPlayer.stop();

                                      await audioPlayer.play(AssetSource(
                                          musicUrlList[index]['path']!));
                                    } else {
                                      if (isPlaying) {
                                        await audioPlayer.pause();
                                      } else {
                                        await audioPlayer.resume();
                                      }
                                    }
                                  },
                                  icon: Icon(
                                      isPlaying && _selected.value == index
                                          ? Icons.pause_rounded
                                          : Icons.play_arrow_rounded)),
                              Expanded(
                                child: Text(
                                  musicUrlList[index]['lable']!,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Text(
                                musicUrlList[index]['duration']!,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider(
                    color: Colors.grey,
                    height: 1,
                  );
                },
                itemCount: musicUrlList.length),
            ValueListenableBuilder(
              valueListenable: _selected,
              builder: (context, state, child) {
                return AnimatedOpacity(
                  opacity: state != null ? 1 : 0,
                  duration: const Duration(milliseconds: 250),
                  child: (_selected.value == null)
                      ? const SizedBox.shrink()
                      : child,
                );
              },
              child: Miniplayer(
                curve: Curves.easeOut,
                minHeight: 70,
                maxHeight: MediaQuery.of(context).size.height,
                controller: miniplayerController,
                builder: (height, percentage) {
                  return AnimatedSwitcher(
                    duration: const Duration(milliseconds: 250),
                    child: (percentage < 0.20)
                        ? Column(
                            children: [
                              ColoredBox(
                                color: Colors.grey,
                                child: IntrinsicHeight(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Container(
                                        color: Colors.white,
                                        child: AlbumPlayerImage(
                                          isPlaying: isPlaying,
                                          child: Image.asset(
                                            'assets/record_.png',
                                            width: 70,
                                            height: 70,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Theme(
                                          data: Theme.of(context).copyWith(
                                            sliderTheme: SliderThemeData(
                                              trackHeight: 5,
                                              thumbColor: Colors.black,
                                              trackShape:
                                                  const RectangularSliderTrackShape(),
                                              overlayShape: SliderComponentShape
                                                  .noOverlay,
                                              thumbShape:
                                                  const RoundSliderThumbShape(
                                                      enabledThumbRadius: 2.5),
                                            ),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Slider(
                                                value: positionDuration
                                                    .inSeconds
                                                    .toDouble(),
                                                min: 0,
                                                max: audioDuration.inSeconds
                                                    .toDouble(),
                                                onChanged: (value) async {
                                                  Duration duration = Duration(
                                                      seconds: value.toInt());

                                                  await audioPlayer
                                                      .seek(duration);
                                                  await audioPlayer.resume();
                                                },
                                              ),
                                              const Spacer(),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 5,
                                                        vertical: 5),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Flexible(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            musicUrlList[_selected
                                                                        .value!]
                                                                    ['lable']
                                                                .toString(),
                                                            maxLines: 1,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                          Text(
                                                            musicUrlList[_selected
                                                                        .value!]
                                                                    ['artist']
                                                                .toString(),
                                                            maxLines: 1,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    IconButton(
                                                      iconSize: 40,
                                                      style:
                                                          IconButton.styleFrom(
                                                        padding:
                                                            EdgeInsets.zero,
                                                        visualDensity:
                                                            VisualDensity
                                                                .compact,
                                                      ),
                                                      onPressed: () async {
                                                        if (isPlaying) {
                                                          await audioPlayer
                                                              .pause();
                                                        } else {
                                                          if (positionDuration ==
                                                              Duration.zero) {
                                                            await audioPlayer
                                                                .stop();

                                                            await audioPlayer.play(
                                                                AssetSource(musicUrlList[
                                                                        _selected
                                                                            .value!]
                                                                    ['path']!));
                                                          } else {
                                                            await audioPlayer
                                                                .resume();
                                                          }
                                                        }
                                                      },
                                                      icon: Icon(
                                                        isPlaying
                                                            ? Icons
                                                                .pause_rounded
                                                            : Icons
                                                                .play_arrow_rounded,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    IconButton(
                                                      iconSize: 40,
                                                      style:
                                                          IconButton.styleFrom(
                                                        padding:
                                                            EdgeInsets.zero,
                                                        visualDensity:
                                                            VisualDensity
                                                                .compact,
                                                      ),
                                                      onPressed: () async {
                                                        await audioPlayer
                                                            .stop();
                                                        _selected.value = null;
                                                      },
                                                      icon: const Icon(
                                                        Icons.close_rounded,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              const Spacer()
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        : SizedBox(
                            height: MediaQuery.of(context).size.height,
                            child: GestureDetector(
                              onTap: () {},
                              child: SingleChildScrollView(
                                physics: const NeverScrollableScrollPhysics(),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Stack(
                                      children: [
                                        SizedBox(
                                          width: double.infinity,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 32, vertical: 16),
                                            color: Colors.grey,
                                            child: AlbumPlayerImage(
                                              isPlaying: isPlaying,
                                              child: Image.asset(
                                                'assets/record_.png',
                                                width: 300,
                                                height: 300,
                                                fit: BoxFit.contain,
                                              ),
                                            ),
                                          ),
                                        ),
                                        IconButton(
                                            onPressed: () {
                                              miniplayerController
                                                  .animateToHeight(
                                                      height: 70,
                                                      duration: const Duration(
                                                        milliseconds: 250,
                                                      ));
                                            },
                                            icon: const Icon(
                                              Icons.arrow_back,
                                              color: Colors.white,
                                            ))
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 32, vertical: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Center(
                                            child: Text(
                                              musicUrlList[_selected.value!]
                                                  ['lable']!,
                                              style: const TextStyle(
                                                  fontSize: 22,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Center(
                                              child: Text(
                                                  musicUrlList[_selected.value!]
                                                      ['artist']!,
                                                  style: const TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.w300))),
                                          const SizedBox(
                                            height: 16,
                                          ),
                                          Theme(
                                            data: Theme.of(context).copyWith(
                                              sliderTheme: SliderThemeData(
                                                trackHeight: 5,
                                                thumbColor: Colors.black,
                                                overlayShape:
                                                    SliderComponentShape
                                                        .noOverlay,
                                                thumbShape:
                                                    const RoundSliderThumbShape(
                                                        enabledThumbRadius: 5),
                                              ),
                                            ),
                                            child: Slider(
                                              value: positionDuration.inSeconds
                                                  .toDouble(),
                                              min: 0,
                                              max: audioDuration.inSeconds
                                                  .toDouble(),
                                              onChanged: (value) async {
                                                Duration duration = Duration(
                                                    seconds: value.toInt());

                                                await audioPlayer
                                                    .seek(duration);
                                                await audioPlayer.resume();
                                              },
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                  formatTime(positionDuration)),
                                              Text(formatTime(audioDuration))
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          ValueListenableBuilder(
                                              valueListenable: _selected,
                                              builder: (context, state, child) {
                                                return Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    IconButton(
                                                        iconSize: 80,
                                                        padding:
                                                            EdgeInsets.zero,
                                                        style: IconButton.styleFrom(
                                                            disabledBackgroundColor:
                                                                Colors.grey,
                                                            backgroundColor:
                                                                Colors.black,
                                                            visualDensity:
                                                                VisualDensity
                                                                    .compact,
                                                            padding: EdgeInsets
                                                                .zero),
                                                        onPressed: state == 0
                                                            ? null
                                                            : () async {
                                                                _selected
                                                                        .value =
                                                                    _selected
                                                                            .value! -
                                                                        1;
                                                                await audioPlayer
                                                                    .stop();

                                                                await audioPlayer.play(
                                                                    AssetSource(
                                                                        musicUrlList[_selected.value!]
                                                                            [
                                                                            'path']!));
                                                              },
                                                        icon: const Icon(
                                                          Icons
                                                              .skip_previous_rounded,
                                                          color: Colors.white,
                                                        )),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: IconButton(
                                                          iconSize: 100,
                                                          padding:
                                                              EdgeInsets.zero,
                                                          style: IconButton.styleFrom(
                                                              backgroundColor:
                                                                  Colors.black,
                                                              visualDensity:
                                                                  VisualDensity
                                                                      .compact,
                                                              padding:
                                                                  EdgeInsets
                                                                      .zero),
                                                          onPressed: () async {
                                                            if (positionDuration ==
                                                                Duration.zero) {
                                                              await audioPlayer
                                                                  .stop();

                                                              await audioPlayer.play(
                                                                  AssetSource(musicUrlList[
                                                                          _selected
                                                                              .value!]
                                                                      [
                                                                      'path']!));
                                                            } else {
                                                              if (isPlaying) {
                                                                await audioPlayer
                                                                    .pause();
                                                              } else {
                                                                await audioPlayer
                                                                    .resume();
                                                              }
                                                            }
                                                          },
                                                          icon: Icon(
                                                            isPlaying
                                                                ? Icons
                                                                    .pause_rounded
                                                                : Icons
                                                                    .play_arrow_rounded,
                                                            color: Colors.white,
                                                          )),
                                                    ),
                                                    IconButton(
                                                        iconSize: 80,
                                                        padding:
                                                            EdgeInsets.zero,
                                                        style: IconButton.styleFrom(
                                                            backgroundColor:
                                                                Colors.black,
                                                            disabledBackgroundColor:
                                                                Colors.grey,
                                                            visualDensity:
                                                                VisualDensity
                                                                    .compact,
                                                            padding: EdgeInsets
                                                                .zero),
                                                        onPressed: state ==
                                                                musicUrlList
                                                                        .length -
                                                                    1
                                                            ? null
                                                            : () async {
                                                                _selected
                                                                        .value =
                                                                    _selected
                                                                            .value! +
                                                                        1;
                                                                await audioPlayer
                                                                    .stop();

                                                                await audioPlayer.play(
                                                                    AssetSource(
                                                                        musicUrlList[_selected.value!]
                                                                            [
                                                                            'path']!));
                                                              },
                                                        icon: const Icon(
                                                          Icons.skip_next_sharp,
                                                          color: Colors.white,
                                                        )),
                                                  ],
                                                );
                                              }),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

String formatTime(Duration duration) {
  String twoDigits(int n) => n.toString().padLeft(2, '0');
  final hours = twoDigits(duration.inHours);
  final minutes = twoDigits(duration.inMinutes.remainder(60));
  final seconds = twoDigits(duration.inSeconds.remainder(60));
  return [
    if (duration.inHours > 0) hours,
    minutes,
    seconds,
  ].join(":");
}
