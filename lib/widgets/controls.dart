import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

class Controls extends StatelessWidget {
  const Controls({
    super.key,
    required this.audioPlayer,
    this.style,
  });
  final AudioPlayer audioPlayer;
  final ButtonStyle? style;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PlayerState>(
        stream: audioPlayer.playerStateStream,
        builder: (context, snapshot) {
          final playerState = snapshot.data;
          final processingState = playerState?.processingState;
          final playing = playerState?.playing;

          if (!(playing ?? false)) {
            return IconButton(
                padding: EdgeInsets.zero,
                style: style ??
                    IconButton.styleFrom(
                        visualDensity: VisualDensity.compact,
                        padding: EdgeInsets.zero),
                onPressed: () async {
                  await audioPlayer.play();
                },
                icon: const Icon(Icons.play_arrow_rounded));
          } else if (processingState != ProcessingState.completed) {
            return IconButton(
                padding: EdgeInsets.zero,
                style: style ??
                    IconButton.styleFrom(
                        visualDensity: VisualDensity.compact,
                        padding: EdgeInsets.zero),
                onPressed: () async {
                  await audioPlayer.pause();
                },
                icon: const Icon(Icons.pause_rounded));
          }
          return IconButton(
              padding: EdgeInsets.zero,
              style: style ??
                  IconButton.styleFrom(
                      visualDensity: VisualDensity.compact,
                      padding: EdgeInsets.zero),
              onPressed: () async {
                final path =
                    ((audioPlayer.sequence?.first as ProgressiveAudioSource?)
                        ?.uri);
                final audioPath = '${path?.origin}${path?.path}';
                await audioPlayer.setUrl(audioPath);
                await audioPlayer.play();
              },
              icon: const Icon(Icons.play_arrow_rounded));
        });
  }
}
