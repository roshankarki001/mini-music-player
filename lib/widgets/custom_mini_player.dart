import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:media_player/album_player.dart';
import 'package:media_player/widgets/audio_model.dart';
import 'package:media_player/widgets/audio_scale.dart';
import 'package:media_player/widgets/controls.dart';
import 'package:miniplayer/miniplayer.dart';

class CustomMiniPlayer extends StatefulWidget {
  final MiniplayerController? miniplayerController;
  final List<AudioData> musicUrlList;
  const CustomMiniPlayer({
    super.key,
    required this.audioPlayer,
    this.miniplayerController,
    required this.musicUrlList,
  });

  final AudioPlayer audioPlayer;

  @override
  State<CustomMiniPlayer> createState() => _CustomMiniPlayerState();
}

class _CustomMiniPlayerState extends State<CustomMiniPlayer> {
  late final MiniplayerController miniplayerController;
  @override
  void initState() {
    super.initState();
    miniplayerController =
        widget.miniplayerController ?? MiniplayerController();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PlayerState>(
        stream: widget.audioPlayer.playerStateStream,
        builder: (context, snapshot) {
          final playerState = snapshot.data;

          final isCompleted =
              playerState?.processingState == ProcessingState.completed;
          final playing = (playerState?.playing ?? false) && !isCompleted;
          final audioPath =
              ((widget.audioPlayer.sequence?.first as ProgressiveAudioSource?)
                      ?.uri
                      .path)
                  ?.split('/')
                  .toList()
                  .last;
          final index = getIndexFromPath(
              audiopath: audioPath, musicList: widget.musicUrlList);
          return Visibility(
            visible: index >= 0,
            child: AnimatedOpacity(
              opacity: (playerState != null &&
                      playerState.processingState.index != 0)
                  ? 1
                  : 0,
              duration: const Duration(milliseconds: 250),
              child: Miniplayer(
                minHeight: 70,
                controller: miniplayerController,
                maxHeight: MediaQuery.of(context).size.height,
                builder: (height, percentage) {
                  return AnimatedSwitcher(
                    duration: const Duration(milliseconds: 250),
                    child: (percentage < 0.20)
                        ? Column(
                            children: [
                              ColoredBox(
                                color: Colors.grey,
                                child: IntrinsicHeight(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      Container(
                                        color: Colors.white,
                                        child: AlbumPlayerImage(
                                          isPlaying: playing,
                                          child: Image.asset(
                                            'assets/record_.png',
                                            width: 70,
                                            height: 70,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              AudioScale(
                                                audioPlayer: widget.audioPlayer,
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Flexible(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          widget
                                                              .musicUrlList[
                                                                  index]
                                                              .lable!,
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                        Text(
                                                          widget
                                                              .musicUrlList[
                                                                  index]
                                                              .artist!,
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Controls(
                                                    audioPlayer:
                                                        widget.audioPlayer,
                                                    style: IconButton.styleFrom(
                                                        foregroundColor:
                                                            Colors.white,
                                                        shape:
                                                            const CircleBorder(),
                                                        iconSize: 40,
                                                        visualDensity:
                                                            VisualDensity
                                                                .compact,
                                                        padding:
                                                            EdgeInsets.zero),
                                                  ),
                                                  IconButton(
                                                    iconSize: 40,
                                                    style: IconButton.styleFrom(
                                                      foregroundColor:
                                                          Colors.white,
                                                      padding: EdgeInsets.zero,
                                                      visualDensity:
                                                          VisualDensity.compact,
                                                      shape:
                                                          const CircleBorder(),
                                                    ),
                                                    onPressed: () async {
                                                      await widget.audioPlayer
                                                          .stop();
                                                    },
                                                    icon: const Icon(
                                                      Icons.close_rounded,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        : SizedBox(
                            height: MediaQuery.of(context).size.height,
                            child: GestureDetector(
                              onTap: () {},
                              child: SingleChildScrollView(
                                physics: const NeverScrollableScrollPhysics(),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Stack(
                                      children: [
                                        SizedBox(
                                          width: double.infinity,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 32, vertical: 16),
                                            color: Colors.grey,
                                            child: AlbumPlayerImage(
                                              isPlaying: playing,
                                              child: Image.asset(
                                                'assets/record_.png',
                                                width: 300,
                                                height: 300,
                                                fit: BoxFit.contain,
                                              ),
                                            ),
                                          ),
                                        ),
                                        IconButton(
                                            onPressed: () async {
                                              miniplayerController
                                                  .animateToHeight(
                                                      height: 70,
                                                      duration: const Duration(
                                                        milliseconds: 250,
                                                      ));
                                            },
                                            icon: const Icon(
                                              Icons.arrow_back,
                                              color: Colors.white,
                                            ))
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 32, vertical: 10),
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                widget
                                                    .musicUrlList[index].lable!,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    fontSize: 22,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                              Text(
                                                widget.musicUrlList[index]
                                                    .artist!,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              const SizedBox(
                                                height: 20,
                                              ),
                                              AudioScale(
                                                audioPlayer: widget.audioPlayer,
                                                timeLabelTextStyle:
                                                    const TextStyle(
                                                        color: Colors.grey),
                                              ),
                                              const SizedBox(
                                                height: 60,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  IconButton(
                                                      padding: EdgeInsets.zero,
                                                      style: IconButton
                                                          .styleFrom(
                                                              iconSize: 60,
                                                              foregroundColor:
                                                                  Colors.white,
                                                              backgroundColor:
                                                                  Colors.black,
                                                              visualDensity:
                                                                  VisualDensity
                                                                      .compact,
                                                              padding:
                                                                  EdgeInsets
                                                                      .zero),
                                                      onPressed: index <= 0
                                                          ? null
                                                          : () async {
                                                              try {
                                                                await widget
                                                                    .audioPlayer
                                                                    .setAsset(widget
                                                                        .musicUrlList[
                                                                            index -
                                                                                1]
                                                                        .path!);
                                                                await widget
                                                                    .audioPlayer
                                                                    .play();
                                                              } catch (ex) {}
                                                            },
                                                      icon: const Icon(Icons
                                                          .skip_previous_rounded)),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  Controls(
                                                    style: IconButton.styleFrom(
                                                      iconSize: 80,
                                                      foregroundColor:
                                                          Colors.white,
                                                      backgroundColor:
                                                          Colors.black,
                                                    ),
                                                    audioPlayer:
                                                        widget.audioPlayer,
                                                  ),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  IconButton(
                                                      padding: EdgeInsets.zero,
                                                      style:
                                                          IconButton.styleFrom(
                                                              iconSize: 60,
                                                              foregroundColor:
                                                                  Colors.white,
                                                              backgroundColor:
                                                                  Colors.black,
                                                              visualDensity:
                                                                  VisualDensity
                                                                      .compact,
                                                              padding: EdgeInsets
                                                                  .zero),
                                                      onPressed: index >=
                                                              widget.musicUrlList
                                                                      .length -
                                                                  1
                                                          ? null
                                                          : () async {
                                                              try {
                                                                await widget
                                                                    .audioPlayer
                                                                    .setAsset(widget
                                                                        .musicUrlList[
                                                                            index +
                                                                                1]
                                                                        .path!);
                                                                await widget
                                                                    .audioPlayer
                                                                    .play();
                                                              } catch (ex) {}
                                                            },
                                                      icon: const Icon(Icons
                                                          .skip_next_rounded)),
                                                ],
                                              )
                                            ])),
                                  ],
                                ),
                              ),
                            ),
                          ),
                  );
                },
              ),
            ),
          );
        });
  }

  int getIndexFromPath(
      {String? audiopath, required List<AudioData> musicList}) {
    if (audiopath == null || musicList.isEmpty) {
      return -1;
    } else {
      var listMusicPath = musicList.map((e) {
        List<String> parts = e.path!.split('/');
        return parts.last.toLowerCase();
      }).toList();
      return listMusicPath.indexOf(audiopath.toLowerCase());
    }
  }
}
